const {makeExecutableSchema} = require('graphql-tools');
import {ggl} from 'apollo-server-express'

const resolvers = require('./resolvers');

const typeDefs = `
  type Link {
    id: ID!
    url: String!
    description: String!
  }

  type Query {
    allLinks: [Link!]!
  }
`;
module.exports = makeExecutableSchema({typeDefs, resolvers});